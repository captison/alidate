# Alidate

Property Validation Methods for Javascript

## What is this?

Well... it's another JS validator package. Yes, there are hundreds of these but many of them are specific to a certain domain or usage.  This one is intended to be more generalized for all purposes.

### Features

- Lots of validation methods
- Both loose (type coerced) and strict (type specific) methods available for some validations
- Easily configure custom validations for list inclusion and regex matching
- No dependencies

## Awesome. How do I get it?

### npm

```
npm install alidate [--save]
```
[![npm version](https://badge.fury.io/js/alidate.svg)](https://badge.fury.io/js/alidate)

### bower

```
bower install alidate [--save]
```
[![bower version](https://badge.fury.io/bo/alidate.svg)](https://badge.fury.io/bo/alidate)

## What's the Setup?

### For Node

```
var alidate = require('alidate');

alidate.configure({ ... });
```
To just use the validation methods directly:
```
var methods = alidate.methods;
```
Simply call the methods as they appear in the _Validation Methods_ section.

For slightly more 'elegant' (or readable) usage:
```
var validate = alidate.validate;
```
With this you can do
```js
var checkName = validate("Alexa").is.not.null().is.LengthMore(16);
// checkName.isValid => false
// checkName.results => { "is.not.null": true, "is.lengthMore": false }
```
Note that `checkName` is actually the same instance originally returned by `validate()` so you can continue calling more validation methods with it.

You can also reset the instance to clear results
```js
checkName.reset();
// checkName.isValid => true
// checkName.results => { }
```
> __NOTE__  
This form of validation is experimental and mostly for fun.  It may change significantly in the future.


### For the Browser

You will find the js for the browser in the **/dist** folder.
```html
<script src="/path/to/alidate.min.js"></script>
```
To use simply configure and then grab the `validate` or `methods` objects.
```html
<script>
    var validate = Alidate.configure({ ... }).validate;
</script>
```
Everything else works the same as in _For Node_ section.

## How does this work?

### Configuration Options

Not much here.

- `lists`  
Setup custom validation methods for item list inclusion (See _The 'in' Methods_).

- `matchers`  
Setup custom validation methods for regular expression matches (See _The 'matches' Methods_).

### Validation Methods

Below are lists of the test methods with short descriptions. Please take a look at the [source](https://bitbucket.org/captison/alidate/src/master/main/methods.js) if you need more clarity on how a particular function works.

> __NOTE__  
The validation methods are designed so that they do not throw errors for the test value.  However, they may throw errors for bad secondary inputs.  As long as any necessary secondary inputs are valid then `true` or `false` will always be returned.

#### The `as` Methods

The following methods make loose `==` comparisons and generally depend on type coercion. Hence they start with 'as' instead of 'is'.

- **asAfterDate** (val, date)  
Returns true if value as a date and comes after the comparison date.

- **asAfterToday** (val)  
Returns true if value as a date and comes after the current date.

- **asBeforeDate** (val, date)  
Returns true if value as a date and comes before the comparison date.

- **asBeforeToday** (val)  
Returns true if value as a date and comes before the current date.

- **asCube** (val)  
Returns true if the cube root of value is a whole number.

- **asDate** (val)  
Returns true if parsing value as a date does not result in NaN.

- **asDivisibleBy** (val, div)  
Returns true if value is not empty and it is evenly divisible by test value.

- **asEqual** (val)  
Returns true if value as equal to the test value.

- **asEqualToDate** (val, date)  
Returns true if value as a date and as equal to the comparison date.

- **asEven** (val)  
Returns true if value is evenly divisible by 2.

- **asFalse** (val)  
Returns true if value is falsey.

- **asFinite** (val)  
Returns true if value is finite.

- **asFractional** (val)  
Returns true if value is not evenly divisible by 1.

- **asInfinite** (val)  
Returns true if value as Infinity.

- **asJson** (val)  
Returns true if value can be parsed as JSON.

- **asLess** (val, cmp)  
Returns true if value is lesser than comparison value.

- **asLessOrEqual** (val, cmp)  
Returns true if value is lesser than or equal to comparison value.

- **asMatch** (val, re)  
Returns true if value matches the regular expression.

- **asMore** (val, cmp)  
Returns true if value is greater than comparison value.

- **asMoreOrEqual** (val, cmp)  
Returns true if value is greater than or equal to comparison value.

- **asNan** (val)  
Returns true if value is NaN.

- **asNull** (val)  
Returns true if value as null.

- **asOdd** (val)  
Returns true if value is not evenly divisible by 2.

- **asPattern** (val)  
Returns true if value as a valid regular expression pattern.

- **asSelf** (val)  
Returns true if value as equal to itself.

- **asSquare** (val)  
Returns true if the square root of value is a whole number.

- **asTrue** (val)  
Returns true if value is truthy.

- **asUndefined** (val)  
Returns true if the value as undefined.

- **asValid** (val)  
Returns true if value is not undefined and as not null.

- **asWhole** (val)  
Returns true if value is evenly divisible by 1.

- **asZero** (val)  
Returns true if value as the number zero.

#### The `has` Methods

These methods deal object and array inclusion.

- **hasConstructor** (val, con)  
Returns true if value has the given constructor.

- **hasKey** (val, key)  
Returns true if value is an object or array and has string key defined.

- **hasValue** (val, find)  
Returns true if value is an object or array and includes the search value.

- **hasUniqueValues** (val, ct)  
Returns true if value is an object or array where no value is repeated more than the number of times (default: 1) specified.

#### The `is` Methods

These methods use strict `===` comparisons and insist on proper types where necessary.

- **isAfterDate** (val, date)  
Returns true if value is a date and comes after the comparison date.

- **isAfterToday** (val)  
Returns true if value is a date and comes after the current date.

- **isAlpha** (val)  
Returns true if the value is a string and contains only letters (case-insensitive).

- **isAlphaNum** (val)  
Returns true if the value is a string and contains only numbers and/or letters (case-insensitive).

- **isArray** (val)  
Returns true if the value is an array.

- **isArrayInstance** (val)  
Returns true if value is an instance of Array.

- **isBeforeDate** (val, date)  
Returns true if value is a date and comes before the comparison date.

- **isBeforeToday** (val)  
Returns true if value is a date and comes before the current date.

- **isBinary** (val)  
Returns true if value is a string and contains only 1's and 0's.

- **isBoolean** (val)  
Returns true if value is a boolean.

- **isBooleanInstance** (val)  
Returns true if value is an instance of Boolean.

- **isBooleanType** (val)  
Returns true if the type of the value is boolean.

- **isCube** (val)  
Returns true if value is a number and its cube root is a whole number.

- **isDate** (val)  
Returns true if value is a date.

- **isDateInstance** (val)  
Returns true if value is an instance of Date.

- **isDecimal** (val)  
Returns true if the value is a string and contains only numbers and one decimal point with at least one digit after the decimal point.

- **isDivisibleBy** (val, div)  
Returns true if value is a number and it is evenly divisible by test value.

- **isDigits** (val)  
Returns true if value is a string and contains only numbers.

- **isEmail** (val)  
Returns true if value is a string and is an email (https://www.w3.org/TR/html5/forms.html#valid-e-mail-address).

- **isEmpty** (val)  
Returns true if value is not valid (undefined or null), has a length property of zero, or is an object with no properties.

- **isEqual** (val)  
Returns true if value is equal to the test value.

- **isEqualToDate** (val, date)  
Returns true if value is a date and is equal to the comparison date.

- **isEven** (val)  
Returns true if value is a number and it is evenly divisible by 2.

- **isFalse** (val)  
Returns true if value is literal false.

- **isFinite** (val)  
Returns true if value is a number and is finite.

- **isFractional** (val)  
Returns true if value is a number and is not evenly divisible by 1.

- **isFunction** (val)  
Returns true if value is a function.

- **isFunctionInstance** (val)  
Returns true if value is an instance of Function.

- **isFunctionType** (val)  
Returns true if the type of the value is function.

- **isHex** (val)  
Returns true if value is a string and contains only numbers and/or letters A to F (case-insensitive).

- **isInfinite** (val)  
Returns true if type of value is number and it is Infinity.

- **isInstanceOf** (val, con)  
Returns true if value has the constructor in its prototype chain.

- **isJson** (val)  
Returns true if value is a string and can be parsed as JSON.

- **isKeyIn** (val, cont)  
Returns true if container is an array or object and includes value as a key.

- **isLength** (val, len)  
Returns true if value is valid and has a 'length' property whose value is equal to given length.

- **isLengthLess** (val, len)  
Returns true if value is valid and has a 'length' property whose value is less than the given length.

- **isLengthLessOrEqual** (val, len)  
Returns true if value is valid and has a 'length' property whose value is less than or equal to the given length.

- **isLengthMore** (val, len)  
Returns true if value is valid and has a 'length' property whose value is greater than the given length.

- **isLengthMoreOrEqual** (val, len)  
Returns true if value is valid and has a 'length' property whose value is greater than or equal to the given length.

- **isLess** (val, cmp)  
Returns true if value is the same type and lesser than comparison value.

- **isLessOrEqual** (val, cmp)  
Returns true if value is the same type and lesser than or equal to comparison value.

- **isMatch** (val, re)  
Returns true if value is a string and matches the regular expression.

- **isMore** (val, cmp)  
Returns true if value is the same type and greater than comparison value.

- **isMoreOrEqual** (val, cmp)  
Returns true if value is the same type and greater than or equal to comparison value.

- **isNan** (val)  
Returns true if value is a number and it is NaN.

- **isNull** (val)  
Returns true if value is null.

- **isNumber** (val)  
Returns true if value is a number.

- **isNumberInstance** (val)  
Returns true if value is an instance of Number.

- **isNumberType** (val)  
Returns true if type of value is number.

- **isObject** (val)  
Returns true if value is an object (not null).

- **isObjectInstance** (val)  
Returns true if value is an instance of Object.

- **isObjectType** (val)  
Returns true if type of value is object.

- **isOdd** (val)  
Returns true if value is a number and it is not evenly divisible by 2.

- **isPattern** (val)  
Returns true if value is a string and is a valid regular expression pattern.

- **isPrime** (val)  
Returns true if type of value is a whole number and evenly divisible only by 1 and itself (http://jsbin.com/zorumagebo/edit?js,console).

- **isPromise** (val)  
Returns true if value is an object and has a .then() method.

- **isRegExp** (val)  
Returns true if value is a regexp.

- **isRegExpInstance** (val)  
Returns true if value is an instance of RegExp.

- **isSelf** (val)  
Returns true if value is equal to itself.

- **isSquare** (val)  
Returns true if value is a number and its square root is a whole number.

- **isString** (val)  
Returns true if value is a string.

- **isStringInstance** (val)  
Returns true if value is an instance of String.

- **isStringType** (val)  
Returns true if type of value is string.

- **isSymbol** (val)  
Returns true if value is a symbol.

- **isSymbolInstance** (val)  
Returns true if value is an instance of Symbol.

- **isSymbolType** (val)  
Returns true if type of value is symbol.

- **isTime** (val)  
Returns true if value can be parsed as a valid date.

- **isTrue** (val)  
Returns true if value is literal true.

- **isTypeMatch** (val, cmp)  
Returns true if type of value is equal to type of compare value.

- **isUndefined** (val)  
Returns true if value is undefined.

- **isUndefinedType** (val)  
Returns true if type of value is undefined.

- **isUrl** (val)  
Returns true if the value is a string and represents a URL that includes an optional protocol and port number (lenient, case-insensitive).

- **isValid** (val)  
Returns true if value is not undefined and not null.

- **isValueIn** (val, cont)  
Returns true if container is an array or object and includes value.

- **isWhitespace** (val)  
Returns true if value is a string and contains only whitespace characters.

- **isWhole** (val)  
Returns true if value is a number and is evenly divisible by 1.

- **isWord** (val)  
Returns true if value is a string and contains only word characters.

- **isZero** (val)  
Returns true if value is the number zero.

### The `in` Methods

These methods are defined by you in the configuraiton.
```
alidate.configure({ lists: [object] });
```
What is `lists`? Here's an example:
```js
{
    stateCodes: [ "CA", "OR", "WA", "NM", "NV", "NY" ],
    orderNumbers: [ 837640238, 725415214, 217605431 ]
}
```
For each item in `lists`, a new validation method is created.  This method will return true if the test value is included in the list.

So now you can
```js
methods.inStateCodes("CA")          // true
methods.inOrderNumbers(348979925)   // false
```
or even
```js
validate("CA").in.stateCodes().isValid          // true
validate(348979925).in.orderNumbers().isValid   // false
```

### The `matches` Methods

These methods are defined by you in the configuraiton.
```
alidate.configure({ matchers: [object] });
```
What is `matchers`? Here's an example:
```js
{
    username: /^[a-z0-9_-]{6,16}$/,
    color: /^#?([a-f0-9]{6})$/
}
```
For each item in `matchers`, a new validation method is created.  This method will return true if the test value is a string and matches the corresponding pattern.

So now you can
```js
methods.matchesUsername("acrobat17")  // true
methods.matchesColor("MAGENTA")       // false
```
or even
```js
validate("acrobat17").matches.username().isValid  // true
validate("MAGENTA").matches.color().isValid       // false
```

### Custom Functions

Custom functions can be added to the methods object via `functions`.
```
alidate.configure({ functions: [object] });
```
Each function should accept the test value as the first argument and return a boolean value.

For example:
```js
{
    creditCard: function(val)
    {
        /* algorithm to verify credit card */
        return ccAlgo.isValid(val); // true/false
    },
    isbn: function(val)
    {
        /* algorithm to verify isbn */
        return isbnAlgo.isValid(val); // true/false
    },
}
```
The custom function is prepended with `fn` in the methods object.
```js
methods.fnCreditCard("4149736294057162")
methods.fnIsbn("1-84356-028-3")
```
And of course, you can also do
```js
validate("4149736294057162").fn.creditCard().isValid
validate("1-84356-028-3").fn.isbn().isValid
```

### Non-Intuitive Points of Interest

With Javascript, things are not always what they seem:

- `NaN` is 'not a number', yet `typeof NaN` reports `'number'`.
- `NaN` is never `===` nor `==` to itself.
- `undefined` is `===` and `==` to itself, but strangely not `<=` nor `>=` to itself.

## What Else?

### Updates

Check out the [changelog](https://bitbucket.org/captison/alidate/src/master/CHANGELOG.md).

### Tests

```
npm test
```

### Feedback

For bugs or feature suggestions please use the [issue tracker](https://bitbucket.org/captison/alidate/issues).

### License

[MIT](https://bitbucket.org/captison/alidate/src/master/LICENSE)

### Finally

Happy Validating!
