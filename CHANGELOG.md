## Alidate Change Log

### In Development (TODOs)


### Releases

#### 0.0.4

- custom functions can now be added to methods object
- .isPromise() added
- .isTime() added

#### 0.0.3

- .hasConstructor() added
- .isKeyIn() and .isValueIn() now aggregate arguments if container arg is not an object
- .isInstanceOf() added
  - .isArrayInstance() added
  - .isBooleanInstance() added
  - .isDateInstance() added
  - .isFunctionInstance() added
  - .isNumberInstance() added
  - .isObjectInstance() added
  - .isRegExpInstance() added
  - .isStringInstance() added
  - .isSymbolInstance() added
- .isInfinite() and .asInfinite() added
- .isFinite() and .asFinite() added
- .isFunction(), .isObject(), .isString(), and .isSymbol() added
- .isNumber() no longer fails for NaN
- .isEmpty() bug fix to work as intended
- regular expression methods
  - .isPattern() and .asPattern() replace .isRegExp() and .asRegExp()
  - .isRegExp() now checks only for a valid RegExp object
  - .asRegExp() removed
- .isNan() and .asNan updated to use global JS isNaN() function
- .isDate() now checks only for a date object, not if date is valid
- .asDate() removed
- fixed issue where .isSelf() method was overwritten by this.self in validate class

#### 0.0.2

- .isObject() bug fixed
- .isBinary() added.
- .hasUniqueValues() added.
- .isAfterToday() and .asAfterToday() added.
- .isBeforeToday() and .asBeforeToday() added.
- .isRegExp() and .asRegExp() added.

#### 0.0.1

- first version of **Alidate**!
