module.exports =
{
    empty:
    {
        value: {},
        tests:
        {
            asFalse: false,
            asJson: false,
            asPattern: true,
            asTrue: true,
            asUndefined: false,
            asZero: false,
            hasConstructor: { args: Array, result: false },
            hasKey: { args: 0, result: false },
            hasValue: { args: 'salami', result: false },
            hasUniqueValues: false,
            isInstanceOf: { args: Object, result: true },
            isArray: false,
            isEmpty: true,
            isFinite: false,
            isFunction: false,
            isFunctionInstance: false,
            isFunctionType: false,
            isInfinite: false,
            isNan: false,
            isObject: true,
            isObjectInstance: true,
            isObjectType: true,
            isValid: true
        }
    },

    lunch:
    {
        value: { main: "cheese enchilada", sideOne: "rice", sideTwo: "beans", drink: "root beer" },
        tests:
        {
            asFalse: false,
            asJson: false,
            asPattern: true,
            asTrue: true,
            asUndefined: false,
            asZero: false,
            hasConstructor: { args: Object, result: true },
            hasKey: { args: 'main', result: true },
            hasValue: { args: 'rice', result: true },
            hasUniqueValues: true,
            isInstanceOf: { args: String, result: false },
            isArray: false,
            isEmpty: false,
            isFinite: false,
            isFunction: false,
            isFunctionInstance: false,
            isFunctionType: false,
            isInfinite: false,
            isNan: false,
            isObject: true,
            isObjectInstance: true,
            isObjectType: true,
            isValid: true
        }
    }
}
