module.exports =
{
    empty:
    {
        value: [],
        tests:
        {
            asFalse: false,
            asJson: false,
            asTrue: true,
            asValid: true,
            asZero: true,
            hasConstructor: { args: Array, result: true },
            hasKey: { args: 0, result: false },
            hasValue: { args: 'bacon', result: false },
            hasUniqueValues: false,
            isArray: true,
            isArrayInstance: true,
            isEmpty: true,
            isJson: false,
            isLengthLessOrEqual: { args: 3, result: true },
            isLengthMore: { args: 3, result: false },
            isObject: false,
            isObjectInstance: true,
            isObjectType: true,
            isValid: true,
            isZero: false
        }
    },

    zero:
    {
        value: [0.0],
        tests:
        {
            asFalse: false,
            asJson: true,
            asTrue: true,
            asValid: true,
            asZero: true,
            hasConstructor: { args: Object, result: false },
            hasKey: { args: 0, result: false },
            hasValue: { args: 'bacon', result: false },
            hasUniqueValues: true,
            isArray: true,
            isEmpty: false,
            isJson: false,
            isLengthLessOrEqual: { args: 3, result: true },
            isLengthMore: { args: 3, result: false },
            isObject: false,
            isObjectInstance: true,
            isObjectType: true,
            isValid: true,
            isZero: false
        }
    },

    breakfast:
    {
        value: ["eggs", "bacon", "bacon", "cheese", "waffles", "taters"],
        tests:
        {
            asFalse: false,
            asJson: false,
            asTrue: true,
            asValid: true,
            asZero: false,
            hasKey: { args: '3', result: true },
            hasValue: { args: 'bacon', result: true },
            hasUniqueValues: false,
            isArray: true,
            isEmpty: false,
            isJson: false,
            isLengthLessOrEqual: { args: 3, result: false },
            isLengthMore: { args: 3, result: true },
            isObject: false,
            isObjectInstance: true,
            isObjectType: true,
            isStringInstance: false,
            isStringType: false,
            isValid: true,
            isZero: false
        }
    },

    numbers:
    {
        value: [7, 87, 9, 3.1415, 101, 23],
        tests:
        {
            asFalse: false,
            asJson: false,
            asTrue: true,
            asValid: true,
            asZero: false,
            hasKey: { args: '3', result: true },
            hasValue: { args: 'bacon', result: false },
            hasUniqueValues: true,
            isArray: true,
            isEmpty: false,
            isJson: false,
            isLengthLessOrEqual: { args: 3, result: false },
            isLengthMore: { args: 3, result: true },
            isObject: false,
            isObjectInstance: true,
            isObjectType: true,
            isValid: true,
            isZero: false
        }
    }
}
