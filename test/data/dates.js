module.exports =
{
    today:
    {
        value: new Date(),
        tests:
        {
            asAfterDate: { args: '2015-10-12', result: true },
            asBeforeDate: { args: '2015-10-12', result: false },
            asNan: false,
            asValid: true,
            isAfterDate: { args: '2015-10-12', result: true },
            isBeforeDate: { args: '2015-10-12', result: false },
            isDate: true,
            isDateInstance: true,
            isNan: false,
            isStringType: false,
            isValid: true,
        }
    },

    valid:
    {
        value: new Date('2015-10-12'),
        tests:
        {
            asAfterDate: { args: '2015-10-12', result: false },
            asBeforeDate: { args: '2015-10-12', result: false },
            asEqualToDate: { args: new Date('2015-10-12'), result: true },
            asNan: false,
            asValid: true,
            isAfterDate: { args: '2015-10-12', result: false },
            isAfterToday: false,
            isBeforeDate: { args: '2015-10-12', result: false },
            isBeforeToday: true,
            isDate: true,
            isEqualToDate: { args: new Date('2015-10-12'), result: true },
            isNan: false,
            isStringType: false,
            isValid: true,
        }
    },

    invalid:
    {
        value: new Date('2014-14-33'),
        tests:
        {
            asAfterDate: { args: '2015-10-12', result: false },
            asBeforeDate: { args: '2015-10-12', result: false },
            asEqualToDate: { args: new Date('2014-14-33'), result: false },
            asMatch: { args: /Invalid Date/, result: true },
            asNan: true,
            asValid: true,
            isAfterDate: { args: '2015-10-12', result: false },
            isAfterToday: false,
            isBeforeDate: { args: '2015-10-12', result: false },
            isBeforeToday: false,
            isDate: true,
            isEqualToDate: { args: new Date('2014-14-33'), result: false },
            isNan: false,
            isStringType: false,
            isValid: true,
        }
    }
}
