
var requireAll = function(filename)
{
    if (__filename.indexOf(filename) != __filename.length - filename.length)
    {
        filename = filename.split('.').slice(0, -1).join('.');
        module.exports[filename] = require("./" + filename);
    }
};

require("fs").readdirSync(__dirname).forEach(requireAll);
