var assert = require('assert');
var validate = require('../main/validate');
var data = require('./data');


function runTests(type, defs, defkeys, testVal, testItems)
{
    defkeys = defkeys ? [].concat(defkeys) : Object.keys(defs);

    for (var i=0,imx=defkeys.length;i<imx;i++)
    {
        var def = defs[defkeys[i]];
        var val = testVal(def);
        var tests = testItems(def);
        var testkeys = Object.keys(tests);

        for (var j=0,jmx=testkeys.length;j<jmx;j++)
        {
            var name = testkeys[j];
            var info = tests[name];
            var expect = typeof info === 'boolean' ? info : info.result;
            var params = typeof info === 'boolean' ? [] : [].concat(info.args);

            var idx = name.search(/[A-Z]/);
            var cat = name.slice(0, idx);
            var not = name.substr(idx, 3) === 'Not' ? 'not' : null;
            var end = name.slice(not ? idx + 3 : idx);
            var sub = end.charAt(0).toLowerCase() + end.slice(1);

            var method = not ? validate(val)[cat][not] : validate(val)[cat];
            var result = method[sub].apply(method, params).isValid;
            var message = "Test " + [type, defkeys[i], name].join(':') + " for (" + val + ") resulted in " + result;

            // console.log(result === expect ? '[ PASS ]' : '[FAILED]', message);
            assert.equal(result, expect, message);
        }
    }
}

module.exports.typeOne = function(type, defkeys)
{
    runTests(type, data[type], defkeys, function(d) { return d.value; }, function(d) { return d.tests; });
}

module.exports.typeTwo = function(type, defkeys)
{
    runTests(type, data[type].values, defkeys, function(d) { return d; }, function(d) { return data[type].tests; });
}
