module.exports =
{
    /**
        Adds an 'in[name]' method to [methods] object that returns true if
        test value is included in [values].

        @param {string} name
            The suffix of the method name (will be capitalized).
        @param {array} values
            The valid values.
        @param {array} methods
            The object in which to add the method.
     */
    addListMethod: function(name, values, methods)
    {
        // ensure values is an array
        var arrValues = Array.isArray(values) ? values : [values];
        // capitalize name
        var capName = this.capitalize(name);
        // add an 'in' method to [methods] that checks for value inclusion
        methods['in' + capName] = function(val)
        {
            return arrValues.indexOf(val) >= 0;
        }
    },

    /**
        Creates list methods on [methods].

        @param {object} lists
            The object (string:array) containing the lists to be added.
        @param {object} methods
            The object in which to add methods.
     */
    addListMethods: function(lists, methods)
    {
        var keys = Object.keys(lists);

        for (var i=0,imx=keys.length;i<imx;i++)
            this.addListMethod(keys[i], lists[keys[i]], methods);
    },

    /**
        Adds an 'matches[name]' method to [methods] object that returns true if
        [regexp] pattern matches the test value.

        @param {string} name
            The suffix of the method name (will be capitalized).
        @param {array} regexp
            The regular expression.
        @param {object} methods
            The object in which to add the method.
     */
    addMatcherMethod: function(name, regexp, methods)
    {
        // compile the regular expression
        var re = new RegExp(regexp);
        // capitalize name
        var capName = this.capitalize(name);
        // add a 'matches' method to [methods] that tests for pattern match
        methods['matches' + capName] = function(val)
        {
            return this.isMatch(val, re);
        }
    },

    /**
        Creates regular expression matcher methods on [methods].

        @param {object} regexps
            The object (string:regexp) containing the patterns to be added.
        @param {object} methods
            The object in which to add methods.
     */
    addMatcherMethods: function(regexps, methods)
    {
        var keys = Object.keys(regexps);

        for (var i=0,imx=keys.length;i<imx;i++)
            this.addMatcherMethod(keys[i], regexps[keys[i]], methods);
    },

    /**
        Adds an 'fn[name]' method to [methods] object.

        @param {string} name
            The suffix of the method name (will be capitalized).
        @param {array} func
            The function to add. This should return a boolean value.
        @param {object} methods
            The object in which to add the method.
     */
    addFunction: function(name, func, methods)
    {
        // capitalize name
        var capName = this.capitalize(name);
        // add the function to [methods]
        methods['fn' + capName] = func;
    },

    /**
        Creates regular expression matcher methods on [methods].

        @param {object} funcs
            The object (string:function) containing the functions to be added.
        @param {object} methods
            The object in which to add methods.
     */
    addFunctions: function(funcs, methods)
    {
        var keys = Object.keys(funcs);

        for (var i=0,imx=keys.length;i<imx;i++)
            this.addFunction(keys[i], funcs[keys[i]], methods);
    },

    capitalize: function(name)
    {
        return name.charAt(0).toUpperCase() + name.slice(1);
    }
}
