module.exports =
{
    reEmail: /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/,
    reUrl: /^([a-z]+:\/\/)?([\da-z-]+\.)+[a-z]+(:\d+)?(\/[^ #]+)?(#[^ #]+)?$/i,

    isJson: function(val)
    {
        try
        {
            JSON.parse(val);
            return true;
        }
        catch (e)
        {
            return false;
        }
    },

    isRegExp: function(val)
    {
        try
        {
            new RegExp(val);
            return true;
        }
        catch (e)
        {
            return false;
        }
    },

    /**
        Loops through the values of [val] sending each value and key to [cb].

        If 'stop' is returned from [cb] the loop will break and false is
        returned.

        @param {array,object} val
            The target collection.
        @param {array,object} cb
            The callback to send each element of [val] to.
        @return {boolean}
            True if the loop completes normally.
     */
    values: function(val, cb)
    {
        var keys = Object.keys(val);

        for (var i=0,imx=keys.length;i<imx;i++)
            if ('stop' === cb(val[keys[i]], keys[i]))
                return false;

        return true;
    }
}
