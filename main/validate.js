var methods = require('./methods');

module.exports = function(val) { return new Validate(val); };

var Validate = function() { this.init.apply(this, arguments); }
Validate.prototype =
{
    init: function(val)
    {
        this.val = val;
        this.reset();

        var keys = Object.keys(proto);
        for (var i=0,imx=keys.length;i<imx;i++)
        {
            this[keys[i]] = new proto[keys[i]](this);
            this[keys[i]].not = new protoNot[keys[i]](this);
        }
    },

    reset: function()
    {
        this.isValid = true;
        this.results = {};

        return this;
    },

    do: function(name, args)
    {
        return methods[name].apply(methods, [].concat([ this.val ], Array.prototype.slice.call(args)));
    },

    track: function(name, result)
    {
        this.results[name] = result;
        this.isValid = this.isValid && this.results[name];
    }
}

var proto = {};
var protoNot = {};

function createMethods(name, cat, sub)
{
    // add validation method to the prototype
    proto[cat] = proto[cat] || function(self) { this.myself = self; };
    proto[cat].prototype[sub] = function()
    {
        this.myself.track([cat, sub].join('.'), this.myself.do(name, arguments));
        return this.myself;
    }
    // add the inverse validation method.
    protoNot[cat] = protoNot[cat] || function(self) { this.myself = self; };
    protoNot[cat].prototype[sub] = function()
    {
        this.myself.track([cat, 'not', sub].join('.'), !this.myself.do(name, arguments));
        return this.myself;
    }
}

var mkeys = Object.keys(methods);
// define prototype methods based on validation methods
for (var i=0,imx=mkeys.length;i<imx;i++)
{
    var name = mkeys[i];
    // extract name parts to 'categorize' prototype methods
    var idx = name.search(/[A-Z]/);
    var cat = name.slice(0, idx);
    var end = name.slice(idx);
    var sub = end.charAt(0).toLowerCase() + end.slice(1);

    createMethods(name, cat, sub);
}
