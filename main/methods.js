
module.exports =
{
    helpers: require('./helpers'),

  /**************************************************************************
    The 'as' validation tests are loose and use '==' for comparison tests.
  **************************************************************************/

    /*
        Returns true if value as a date and comes after the comparison date.
     */
    asAfterDate: function(val, date)
    {
        return Date.parse(val) > Date.parse(date);
    },

    /*
        Returns true if value as a date and comes after the current date.
     */
    asAfterToday: function(val)
    {
        return this.asAfterDate(val, new Date());
    },

    /*
        Returns true if value as a date and comes before the comparison date.
     */
    asBeforeDate: function(val, date)
    {
        return Date.parse(val) < Date.parse(date);
    },

    /*
        Returns true if value as a date and comes before the current date.
     */
    asBeforeToday: function(val)
    {
        return this.asBeforeDate(val, new Date());
    },

    /*
        Returns true if the cube root of value is a whole number.
     */
    asCube: function(val)
    {
        return this.asWhole(Math.cbrt(val));
    },

    /*
        Returns true if value is not empty and it is evenly divisible by
        test value.
     */
    asDivisibleBy: function(val, tst)
    {
        return !this.isEmpty(val) && val % tst == 0;
    },

    /*
        Returns true if value as an email
        (https://www.w3.org/TR/html5/forms.html#valid-e-mail-address).
     */
    asEmail: function(val)
    {
        return this.asMatch(val, this.helpers.reEmail);
    },

    /*
        Returns true if value as equal to the test value.
     */
    asEqual: function(val, tst)
    {
        return val == tst;
    },

    /*
        Returns true if value as a date and as equal to the comparison date.
     */
    asEqualToDate: function(val, date)
    {
        return Date.parse(val) == Date.parse(date);
    },

    /*
        Returns true if value is evenly divisible by 2.
     */
    asEven: function(val)
    {
        return val % 2 == 0;
    },

    /*
        Returns true if value is falsey.
     */
    asFalse: function(val)
    {
        return val ? false : true;
    },

    /*
        Returns true if value is finite.
     */
    asFinite: function(val)
    {
        return isFinite(val);
    },

    /*
        Returns true if value is not evenly divisible by 1.
     */
    asFractional: function(val)
    {
        return val % 1 != 0;
    },

    /*
        Returns true if value as Infinity.
     */
    asInfinite: function(val)
    {
        return val == Infinity;
    },

    /*
        Returns true if value can be parsed as JSON.
     */
    asJson: function(val)
    {
        return this.helpers.isJson(val);
    },

    /*
        Returns true if value is lesser than comparison value.
     */
    asLess: function(val, cmp)
    {
        return val < cmp;
    },

    /*
        Returns true if value is lesser than or equal to comparison value.
     */
    asLessOrEqual: function(val, cmp)
    {
        return val <= cmp;
    },

    /*
        Returns true if value matches the regular expression.
     */
    asMatch: function(val, re)
    {
        return re.test(val);
    },

    /*
        Returns true if value is greater than comparison value.
     */
    asMore: function(val, cmp)
    {
        return val > cmp;
    },

    /*
        Returns true if value is greater than or equal to comparison value.
     */
    asMoreOrEqual: function(val, cmp)
    {
        return val >= cmp;
    },

    /*
        Returns true if value is NaN.
     */
    asNan: function(val)
    {
        return isNaN(val);
    },

    /*
        Returns true if value as null.
     */
    asNull: function(val)
    {
        return val == null;
    },

    /*
        Returns true if value is not evenly divisible by 2.
     */
    asOdd: function(val)
    {
        return val % 2 == 1;
    },

    /*
        Returns true if value as a valid regular expression pattern.
     */
    asPattern: function(val)
    {
        return this.helpers.isRegExp(val);
    },

    /*
        Returns true if value as equal to itself.
     */
    asSelf: function(val)
    {
        return val == val;
    },

    /*
        Returns true if the square root of value is a whole number.
     */
    asSquare: function(val)
    {
        return this.asWhole(Math.sqrt(val));
    },

    /*
        Returns true if value is truthy.
     */
    asTrue: function(val)
    {
        return val ? true : false;
    },

    /*
        Returns true if the value as undefined.
     */
    asUndefined: function(val)
    {
        return val == undefined;
    },

    /*
        Returns true if value is not undefined and as not null.
     */
    asValid: function(val)
    {
        return !this.isUndefined(val) && !this.asNull(val);
    },

    /*
        Returns true if value is evenly divisible by 1.
     */
    asWhole: function(val)
    {
        return val % 1 == 0;
    },

    /*
        Returns true if value as the number zero.
     */
    asZero: function(val)
    {
        return val == 0;
    },


    /**************************************************************************
      The 'has' validation tests check for inclusion.
    **************************************************************************/

    /*
        Returns true if value has the given constructor.
     */
    hasConstructor: function(val, con)
    {
        return this.isValid(val) && val.constructor === con;
    },

    /*
        Returns true if value is an object or array and has string key defined.

        Note that the keys are always strings here, even for arrays. So, to
        check if array has index 3, pass [key] as '3'.
     */
    hasKey: function(val, key)
    {
        if (this.isEmpty(val) || !(this.isObject(val) || this.isArray(val)))
            return false;

        return !this.helpers.values(val, function(item, valKey)
        {
            if (valKey === key) return 'stop';
        });
    },

    /*
        Returns true if value is an object or array and includes the search
        value.

        Note arrays and objects are not equal unless they are the exact same
        object.
     */
    hasValue: function(val, find)
    {
        if (this.isEmpty(val) || !(this.isObject(val) || this.isArray(val)))
            return false;

        return !this.helpers.values(val, function(item)
        {
            if (item === find) return 'stop';
        });
    },

    /*
        Returns true if value is an object or array where no value is repeated
        more than the number of times (default: 1) specified.

        Note that object items in [val] are considered to be equal as this is
        not a deep check.
     */
    hasUniqueValues: function(val, ct)
    {
        if (this.isEmpty(val) || !(this.isObject(val) || this.isArray(val)))
            return false;

        ct = this.isNumber(ct) ? ct : 1;
        var counts = {};
        return this.helpers.values(val, function(item)
        {
            counts[item] = (counts[item] || 0) + 1;
            if (counts[item] > ct) return 'stop';
        });
    },

    /**************************************************************************
      The 'is' validation tests are strict and use '===' for comparison tests.
    **************************************************************************/

    /*
        Returns true if value is a date and comes after the comparison date.
     */
    isAfterDate: function(val, date)
    {
        return this.isDate(val) && val.getTime() > Date.parse(date);
    },

    /*
        Returns true if value is a date and comes after the current date.
     */
    isAfterToday: function(val)
    {
        return this.isAfterDate(val, new Date());
    },

    /*
        Returns true if the value is a string and contains only letters
        (case-insensitive).
     */
    isAlpha: function(val)
    {
        return this.isMatch(val, /^[A-Z]+$/i);
    },

    /*
        Returns true if the value is a string and contains only numbers and/or
        letters (case-insensitive).
     */
    isAlphaNum: function(val)
    {
        return this.isMatch(val, /^[0-9A-Z]+$/i);
    },

    /*
        Returns true if the value is an array.
     */
    isArray: function(val)
    {
        return Array.isArray ? Array.isArray(val) : Object.prototype.toString.call(val) === '[object Array]';
    },

    /*
        Returns true if value is an instance of Array.
     */
    isArrayInstance: function(val)
    {
        return this.isInstanceOf(val, Array);
    },

    /*
        Returns true if value is a date and comes before the comparison date.
     */
    isBeforeDate: function(val, date)
    {
        return this.isDate(val) && val.getTime() < Date.parse(date);
    },

    /*
        Returns true if value is a date and comes before the current date.
     */
    isBeforeToday: function(val)
    {
        return this.isBeforeDate(val, new Date());
    },

    /*
        Returns true if value is a string and contains only 1's and 0's.
     */
    isBinary: function(val)
    {
        return this.isMatch(val, /^[01]+$/);
    },

    /*
        Returns true if value is a boolean.
     */
    isBoolean: function(val)
    {
        return Object.prototype.toString.call(val) === '[object Boolean]';
    },

    /*
        Returns true if value is an instance of Boolean.
     */
    isBooleanInstance: function(val)
    {
        return this.isInstanceOf(val, Boolean);
    },

    /*
        Returns true if the type of the value is boolean.
     */
    isBooleanType: function(val)
    {
        return typeof val === 'boolean';
    },

    /*
        Returns true if value is a number and its cube root is a whole number.
     */
    isCube: function(val)
    {
        return this.isNumber(val) && this.isWhole(Math.cbrt(val));
    },

    /*
        Returns true if value is a date.
     */
    isDate: function(val)
    {
        return Object.prototype.toString.call(val) === '[object Date]';
    },

    /*
        Returns true if value is an instance of Date.
     */
    isDateInstance: function(val)
    {
        return this.isInstanceOf(val, Date);
    },

    /*
        Returns true if the value is a string and contains only numbers
        and one decimal point with at least one digit after the decimal
        point.
     */
    isDecimal: function(val)
    {
        return this.isMatch(val, /^\d*[.]\d+$/);
    },

    /*
        Returns true if value is a number and it is evenly divisible by
        test value.
     */
    isDivisibleBy: function(val, tst)
    {
        return this.isNumber(val) && val % tst === 0;
    },

    /*
        Returns true if value is a string and contains only numbers.
     */
    isDigits: function(val)
    {
        return this.isMatch(val, /^\d+$/);
    },

    /*
        Returns true if value is a string and is an email
        (https://www.w3.org/TR/html5/forms.html#valid-e-mail-address).
     */
    isEmail: function(val)
    {
        return this.isMatch(val, this.helpers.reEmail);
    },

    /*
        Returns true if
            - value is not valid (undefined or null)
            - value has a length property of zero
            - value is an object with no properties
     */
    isEmpty: function(val)
    {
        switch (true)
        {
            case !this.isValid(val):
            case this.isValid(val.length) && val.length === 0:
            case this.isObject(val) && Object.keys(val).length === 0:
              return true;
        }

        return false;
    },

    /*
        Returns true if value is equal to the test value.
     */
    isEqual: function(val, tst)
    {
        return val === tst;
    },

    /*
        Returns true if value is a date and is equal to the comparison date.
     */
    isEqualToDate: function(val, date)
    {
        return this.isDate(val) && val.getTime() === Date.parse(date);
    },

    /*
        Returns true if value is a number and it is evenly divisible by 2.
     */
    isEven: function(val)
    {
        return this.isNumber(val) && val % 2 === 0;
    },

    /*
        Returns true if value is literal false.
     */
    isFalse: function(val)
    {
        return val === false;
    },

    /*
        Returns true if value is a number and is finite.
     */
    isFinite: function(val)
    {
        return this.isNumber(val) && isFinite(val);
    },

    /*
        Returns true if value is a number and is not evenly divisible by 1.
     */
    isFractional: function(val)
    {
        return this.isNumber(val) && val % 1 !== 0;
    },

    /*
        Returns true if value is a function.
     */
    isFunction: function(val)
    {
        return Object.prototype.toString.call(val) === '[object Function]';
    },

    /*
        Returns true if value is an instance of Function.
     */
    isFunctionInstance: function(val)
    {
        return this.isInstanceOf(val, Function);
    },

    /*
        Returns true if the type of the value is function.
     */
    isFunctionType: function(val)
    {
        return typeof val === 'function';
    },

    /*
        Returns true if value is a string and contains only numbers and/or letters
        A to F (case-insensitive).
     */
    isHex: function(val)
    {
        return this.isMatch(val, /^[0-9A-F]+$/i);
    },

    /*
        Returns true if value is a number and it is Infinity.
     */
    isInfinite: function(val)
    {
        return this.isNumber(val) && val === Infinity;
    },

    /*
        Returns true if value has the constructor in its prototype chain.
     */
    isInstanceOf: function(val, con)
    {
        return val instanceof con;
    },

    /*
        Returns true if value is a string and can be parsed as JSON.
     */
    isJson: function(val)
    {
        return this.isString(val) && this.helpers.isJson(val);
    },

    /*
        Returns true if container is an array or object and includes value as
        a key.
     */
    isKeyIn: function(val, cont)
    {
        if (this.isArray(cont) || this.isObject(cont))
        {
            return !this.helpers.values(Object.keys(cont), function(value, key)
            {
                if (key === val) return 'stop';
            });
        }

        return Array.prototype.slice.call(arguments).slice(1).indexOf(val) >= 0;
    },

    /*
        Returns true if value is valid and has a 'length' property whose value
        is equal to test length.
     */
    isLength: function(val, len)
    {
        return this.isValid(val) && this.isValid(val.length) ? val.length === len : false;
    },

    /*
        Returns true if value is valid and has a 'length' property whose value
        is less than the given length.
     */
    isLengthLess: function(val, len)
    {
        return this.isValid(val) && this.isValid(val.length) ? val.length < len : false;
    },

    /*
        Returns true if value is valid and has a 'length' property whose value
        is less than or equal to the given length.
     */
    isLengthLessOrEqual: function(val, len)
    {
        return this.isValid(val) && this.isValid(val.length) ? val.length <= len : false;
    },

    /*
        Returns true if value is valid and has a 'length' property whose value
        is greater than the given length.
     */
    isLengthMore: function(val, len)
    {
        return this.isValid(val) && this.isValid(val.length) ? val.length > len : false;
    },

    /*
        Returns true if value is valid and has a 'length' property whose value
        is greater than or equal to the given length.
     */
    isLengthMoreOrEqual: function(val, len)
    {
        return this.isValid(val) && this.isValid(val.length) ? val.length >= len : false;
    },

    /*
        Returns true if value is the same type and lesser than comparison
        value.
     */
    isLess: function(val, cmp)
    {
        return this.isTypeMatch(val, cmp) && val < cmp;
    },

    /*
        Returns true if value is the same type and lesser than or equal to
        comparison value.
     */
    isLessOrEqual: function(val, cmp)
    {
        return this.isTypeMatch(val, cmp) && val <= cmp;
    },

    /*
        Returns true if value is a string and matches the regular expression.
     */
    isMatch: function(val, re)
    {
        return this.isString(val) && re.test(val);
    },

    /*
        Returns true if value is the same type and greater than comparison
        value.
     */
    isMore: function(val, cmp)
    {
        return this.isTypeMatch(val, cmp) && val > cmp;
    },

    /*
        Returns true if value is the same type and greater than or equal to
        comparison value.
     */
    isMoreOrEqual: function(val, cmp)
    {
        return this.isTypeMatch(val, cmp) && val >= cmp;
    },

    /*
        Returns true if value is a number and it is NaN.
     */
    isNan: function(val)
    {
        return this.isNumber(val) && isNaN(val);
    },

    /*
        Returns true if value is null.
     */
    isNull: function(val)
    {
        return val === null;
    },

    /*
        Returns true if value is a number.
     */
    isNumber: function(val)
    {
        return Object.prototype.toString.call(val) === '[object Number]';
    },

    /*
        Returns true if value is an instance of Number.
     */
    isNumberInstance: function(val)
    {
        return this.isInstanceOf(val, Number);
    },

    /*
        Returns true if type of value is number.
     */
    isNumberType: function(val)
    {
        return typeof val === 'number';
    },

    /*
        Returns true if value is an object (not null).
     */
    isObject: function(val)
    {
        return Object.prototype.toString.call(val) === '[object Object]';
    },

    /*
        Returns true if value is an instance of Object.
     */
    isObjectInstance: function(val)
    {
        return this.isInstanceOf(val, Object);
    },

    /*
        Returns true if type of value is object.
     */
    isObjectType: function(val)
    {
        return typeof val === 'object';
    },

    /*
        Returns true if value is a number and it is not evenly divisible by 2.
     */
    isOdd: function(val)
    {
        return this.isNumber(val) && val % 2 === 1;
    },

    /*
        Returns true if value is a string and is a valid regular expression
        pattern.
     */
    isPattern: function(val)
    {
        return this.isString(val) && this.helpers.isRegExp(val);
    },

    /*
        Returns true if value is a whole number and evenly divisible only by 1
        and itself (http://jsbin.com/zorumagebo/edit?js,console).
     */
    isPrime: function(val)
    {
        return this.isWhole(val) && !(Array(Math.abs(val) + 1).join(1).match(/^1?$|^(11+?)\1+$/));
    },

    /*
        Returns true if value is an object and has a .then() method.
     */
    isPromise: function(val)
    {
        return this.isObject(val) && this.isFunction(val.then);
    },

    /*
        Returns true if value is a regexp.
     */
    isRegExp: function(val)
    {
        return Object.prototype.toString.call(val) === '[object RegExp]';
    },

    /*
        Returns true if value is an instance of RegExp.
     */
    isRegExpInstance: function(val)
    {
        return this.isInstanceOf(val, RegExp);
    },

    /*
        Returns true if value is equal to itself.
     */
    isSelf: function(val)
    {
        return val === val;
    },

    /*
        Returns true if value is a number and its square root is a whole
        number.
     */
    isSquare: function(val)
    {
        return this.isNumber(val) && this.isWhole(Math.sqrt(val));
    },

    /*
        Returns true if value is a string.
     */
    isString: function(val)
    {
        return Object.prototype.toString.call(val) === '[object String]';
    },

    /*
        Returns true if value is an instance of String.
     */
    isStringInstance: function(val)
    {
        return this.isInstanceOf(val, String);
    },

    /*
        Returns true if the type of the value is string.
     */
    isStringType: function(val)
    {
        return typeof val === 'string';
    },

    /*
        Returns true if value is a symbol.
     */
    isSymbol: function(val)
    {
        return Object.prototype.toString.call(val) === '[object Symbol]';
    },

    /*
        Returns true if value is an instance of Symbol.
     */
    isSymbolInstance: function(val)
    {
        return this.isInstanceOf(val, Symbol);
    },

    /*
        Returns true if the type of the value is symbol.
     */
    isSymbolType: function(val)
    {
        return typeof val === 'symbol';
    },

    /*
        Returns true if value can be parsed as a valid date.
     */
    isTime: function(val)
    {
        return !this.isNan(Date.parse(val));
    },

    /*
        Returns true if value is literal true.
     */
    isTrue: function(val)
    {
        return val === true;
    },

    /*
        Returns true if type of value is equal to type of compare value.
     */
    isTypeMatch: function(val, cmp)
    {
        return typeof val === typeof cmp;
    },

    /*
        Returns true if value is undefined.
     */
    isUndefined: function(val)
    {
        return val === undefined;
    },

    /*
        Returns true if type of value is undefined.
     */
    isUndefinedType: function(val)
    {
        return typeof val === 'undefined';
    },

    /*
        Returns true if the value is a string and represents a URL that includes as
        optional protocol and port number (lenient, case-insensitive).

        Note that protocol can be any alpha string.
     */
    isUrl: function(val)
    {
       return this.isMatch(val, this.helpers.reUrl);
    },

    /*
        Returns true if value is not undefined and not null.
     */
    isValid: function(val)
    {
        return !this.isUndefined(val) && !this.isNull(val);
    },

    /*
        Returns true if container is an array or object or if any additional
        parameters to this method includes value.
     */
    isValueIn: function(val, cont)
    {
        if (this.isArray(cont) || this.isObject(cont))
        {
            return !this.helpers.values(Object.keys(cont), function(value, key)
            {
                if (cont[value] === val) return 'stop';
            });
        }

        return Array.prototype.slice.call(arguments).slice(1).indexOf(val) >= 0;
    },

    /*
        Returns true if value is a string and contains only whitespace
        characters.
     */
    isWhitespace: function(val)
    {
        return this.isMatch(val, /^\s+$/);
    },

    /*
        Returns true if value is a number and is evenly divisible by 1.
     */
    isWhole: function(val)
    {
        return this.isNumber(val) && val % 1 === 0;
    },

    /*
        Returns true if value is a string and contains only word characters.
     */
    isWord: function(val)
    {
        return this.isMatch(val, /^\w+$/);
    },

    /*
        Returns true if value is the number zero.
     */
    isZero: function(val)
    {
        return val === 0;
    }
}
