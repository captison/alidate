var dynamics = require('./dynamics');

Alidate =
{
    validate: require('./validate'),
    methods: require('./methods'),

    configure: function(options)
    {
        // add list methods
        dynamics.addListMethods(options.lists || {}, this.methods);
        // add matcher methods
        dynamics.addMatcherMethods(options.matchers || {}, this.methods)
        // add custom functions
        dynamics.addFunctions(options.functions || {}, this.methods)

        return this;
    }
}

module.exports = Alidate;
